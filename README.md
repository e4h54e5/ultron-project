<div align="center"><img src="https://static.wikia.nocookie.net/marvelcinematicuniverse/images/7/74/Ultron_Program_8.7.png/revision/latest?cb=20160211165928"></div>
<h1 align="center">Project ULTRON</h1>
<p align="center"><strong>Semi-malicious, yet completely unharmful ULTRON chatbo.</strong>
<br/>
<h2>About</h2>
Simple artificial intelligent chatbot that responds to user input.

<h2>Goal and requirements</h2>
Represent: for-loop iteration, while-loop iteration, if-statements, and string modifiers.

<h2>Key learnings</h2>

- For Loop Iteration in Java
- While Loop Iteration in Java
- Logical If-Statements
- Nested If-Statements
- Java String Modifiers

<h2>How To Use</h2>

Launch the executable in <a href="https://gitlab.com/e4h54e5/ultron-project/-/archive/main/ultron-project-main.zip">./build/ultron-ai.exe</a>.

<h2>Project status</h2>
Currently Not Maintained

<h2>Credits</h2>

- Author is redacted.

<h2>Copyright</h2>
This project is licensed under the terms of the MIT license and protected by Udacity Honor Code and Community Code of Conduct. See <a href="LICENSE.md">license</a> and <a href="LICENSE.DISCLAIMER.md">disclaimer</a>.
