﻿
namespace ultron
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.checkPost = new System.Windows.Forms.Timer(this.components);
            this.audioVisualization = new CSAudioVisualization.AudioVisualization();
            this.topLBorder = new System.Windows.Forms.Panel();
            this.leftBorder = new System.Windows.Forms.Panel();
            this.bottomBorder = new System.Windows.Forms.Panel();
            this.rightBorder = new System.Windows.Forms.Panel();
            this.topRBorder = new System.Windows.Forms.Panel();
            this.exitButton = new System.Windows.Forms.Label();
            this.headerBorder = new System.Windows.Forms.Panel();
            this.textboxBorder = new System.Windows.Forms.Panel();
            this.sendButton = new System.Windows.Forms.Label();
            this.headerText = new System.Windows.Forms.Label();
            this.asciiArt = new System.Windows.Forms.PictureBox();
            this.inputBox = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.mainWindow = new System.Windows.Forms.Panel();
            this.informationBox = new ultron.InfoBox();
            this.windowName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.asciiArt)).BeginInit();
            this.mainWindow.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkPost
            // 
            this.checkPost.Interval = 1500;
            this.checkPost.Tick += new System.EventHandler(this.checkPost_Tick);
            // 
            // audioVisualization
            // 
            this.audioVisualization.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.audioVisualization.AudioSource = "Blue Snowball (Blue Snowball)";
            this.audioVisualization.BarCount = 50;
            this.audioVisualization.BarSpacing = 2;
            this.audioVisualization.ColorBase = System.Drawing.Color.DimGray;
            this.audioVisualization.ColorMax = System.Drawing.Color.LightGray;
            this.audioVisualization.DeviceIndex = 0;
            this.audioVisualization.FileName = null;
            this.audioVisualization.HighQuality = false;
            this.audioVisualization.Interval = 30;
            this.audioVisualization.IsXLogScale = false;
            this.audioVisualization.Location = new System.Drawing.Point(239, 204);
            this.audioVisualization.Margin = new System.Windows.Forms.Padding(4);
            this.audioVisualization.MaximumFrequency = 10000;
            this.audioVisualization.MessageArgs = null;
            this.audioVisualization.Mode = CSAudioVisualization.Mode.WasapiCapture;
            this.audioVisualization.Name = "audioVisualization";
            this.audioVisualization.pic3DGraph = null;
            this.audioVisualization.Size = new System.Drawing.Size(888, 254);
            this.audioVisualization.TabIndex = 11;
            this.audioVisualization.UseAverage = true;
            this.audioVisualization.UserKey = "Your registration key";
            this.audioVisualization.UserName = "Your email";
            this.audioVisualization.VisMode = CSAudioVisualization.GraphMode.ModeSpectrum;
            // 
            // topLBorder
            // 
            this.topLBorder.BackColor = System.Drawing.Color.DimGray;
            this.topLBorder.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.topLBorder.Location = new System.Drawing.Point(0, 0);
            this.topLBorder.Name = "topLBorder";
            this.topLBorder.Size = new System.Drawing.Size(270, 1);
            this.topLBorder.TabIndex = 0;
            // 
            // leftBorder
            // 
            this.leftBorder.BackColor = System.Drawing.Color.DimGray;
            this.leftBorder.Dock = System.Windows.Forms.DockStyle.Left;
            this.leftBorder.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.leftBorder.Location = new System.Drawing.Point(0, 0);
            this.leftBorder.Name = "leftBorder";
            this.leftBorder.Size = new System.Drawing.Size(1, 325);
            this.leftBorder.TabIndex = 2;
            // 
            // bottomBorder
            // 
            this.bottomBorder.BackColor = System.Drawing.Color.DimGray;
            this.bottomBorder.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomBorder.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bottomBorder.Location = new System.Drawing.Point(1, 324);
            this.bottomBorder.Name = "bottomBorder";
            this.bottomBorder.Size = new System.Drawing.Size(487, 1);
            this.bottomBorder.TabIndex = 3;
            // 
            // rightBorder
            // 
            this.rightBorder.BackColor = System.Drawing.Color.DimGray;
            this.rightBorder.Dock = System.Windows.Forms.DockStyle.Right;
            this.rightBorder.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.rightBorder.Location = new System.Drawing.Point(487, 0);
            this.rightBorder.Name = "rightBorder";
            this.rightBorder.Size = new System.Drawing.Size(1, 324);
            this.rightBorder.TabIndex = 4;
            // 
            // topRBorder
            // 
            this.topRBorder.BackColor = System.Drawing.Color.DimGray;
            this.topRBorder.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.topRBorder.Location = new System.Drawing.Point(458, 0);
            this.topRBorder.Name = "topRBorder";
            this.topRBorder.Size = new System.Drawing.Size(30, 1);
            this.topRBorder.TabIndex = 5;
            // 
            // exitButton
            // 
            this.exitButton.AutoSize = true;
            this.exitButton.Cursor = System.Windows.Forms.Cursors.No;
            this.exitButton.ForeColor = System.Drawing.Color.DimGray;
            this.exitButton.Location = new System.Drawing.Point(474, 2);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(12, 13);
            this.exitButton.TabIndex = 8;
            this.exitButton.Text = "x";
            this.exitButton.DoubleClick += new System.EventHandler(this.RequestExit);
            // 
            // headerBorder
            // 
            this.headerBorder.BackColor = System.Drawing.Color.DimGray;
            this.headerBorder.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.headerBorder.Location = new System.Drawing.Point(6, 25);
            this.headerBorder.Name = "headerBorder";
            this.headerBorder.Size = new System.Drawing.Size(250, 1);
            this.headerBorder.TabIndex = 9;
            // 
            // textboxBorder
            // 
            this.textboxBorder.BackColor = System.Drawing.Color.DimGray;
            this.textboxBorder.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.textboxBorder.Location = new System.Drawing.Point(6, 293);
            this.textboxBorder.Name = "textboxBorder";
            this.textboxBorder.Size = new System.Drawing.Size(477, 1);
            this.textboxBorder.TabIndex = 10;
            // 
            // sendButton
            // 
            this.sendButton.AutoSize = true;
            this.sendButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.sendButton.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendButton.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.sendButton.Location = new System.Drawing.Point(407, 303);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(61, 11);
            this.sendButton.TabIndex = 11;
            this.sendButton.Text = "[ Send ]";
            // 
            // headerText
            // 
            this.headerText.AutoSize = true;
            this.headerText.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.headerText.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.headerText.Location = new System.Drawing.Point(7, 8);
            this.headerText.Name = "headerText";
            this.headerText.Size = new System.Drawing.Size(96, 11);
            this.headerText.TabIndex = 8;
            this.headerText.Text = "Good morning.";
            // 
            // asciiArt
            // 
            this.asciiArt.Image = ((System.Drawing.Image)(resources.GetObject("asciiArt.Image")));
            this.asciiArt.Location = new System.Drawing.Point(273, 20);
            this.asciiArt.Name = "asciiArt";
            this.asciiArt.Size = new System.Drawing.Size(196, 261);
            this.asciiArt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.asciiArt.TabIndex = 12;
            this.asciiArt.TabStop = false;
            // 
            // inputBox
            // 
            this.inputBox.BackColor = System.Drawing.Color.Black;
            this.inputBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.inputBox.Font = new System.Drawing.Font("Lucida Console", 8F);
            this.inputBox.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.inputBox.Location = new System.Drawing.Point(24, 303);
            this.inputBox.MaxLength = 30;
            this.inputBox.Multiline = false;
            this.inputBox.Name = "inputBox";
            this.inputBox.Size = new System.Drawing.Size(372, 18);
            this.inputBox.TabIndex = 120;
            this.inputBox.Text = "";
            this.inputBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.inputBox_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lucida Console", 8F);
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label1.Location = new System.Drawing.Point(10, 303);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 11);
            this.label1.TabIndex = 18;
            this.label1.Text = ">";
            // 
            // mainWindow
            // 
            this.mainWindow.BackColor = System.Drawing.Color.Black;
            this.mainWindow.Controls.Add(this.informationBox);
            this.mainWindow.Controls.Add(this.label1);
            this.mainWindow.Controls.Add(this.inputBox);
            this.mainWindow.Controls.Add(this.asciiArt);
            this.mainWindow.Controls.Add(this.headerText);
            this.mainWindow.Controls.Add(this.sendButton);
            this.mainWindow.Controls.Add(this.textboxBorder);
            this.mainWindow.Controls.Add(this.headerBorder);
            this.mainWindow.Controls.Add(this.exitButton);
            this.mainWindow.Controls.Add(this.topRBorder);
            this.mainWindow.Controls.Add(this.rightBorder);
            this.mainWindow.Controls.Add(this.bottomBorder);
            this.mainWindow.Controls.Add(this.leftBorder);
            this.mainWindow.Controls.Add(this.topLBorder);
            this.mainWindow.Location = new System.Drawing.Point(439, 210);
            this.mainWindow.Name = "mainWindow";
            this.mainWindow.Size = new System.Drawing.Size(488, 325);
            this.mainWindow.TabIndex = 12;
            // 
            // informationBox
            // 
            this.informationBox.BackColor = System.Drawing.Color.Black;
            this.informationBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.informationBox.Font = new System.Drawing.Font("Lucida Console", 8F);
            this.informationBox.ForeColor = System.Drawing.Color.White;
            this.informationBox.Location = new System.Drawing.Point(6, 32);
            this.informationBox.Name = "informationBox";
            this.informationBox.ReadOnly = true;
            this.informationBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.informationBox.Size = new System.Drawing.Size(250, 249);
            this.informationBox.TabIndex = 14;
            this.informationBox.Text = "";
            // 
            // windowName
            // 
            this.windowName.AutoSize = true;
            this.windowName.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.windowName.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.windowName.Location = new System.Drawing.Point(712, 204);
            this.windowName.Name = "windowName";
            this.windowName.Size = new System.Drawing.Size(187, 11);
            this.windowName.TabIndex = 13;
            this.windowName.Text = "[ultron ui - version b1.0]";
            this.windowName.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragStart);
            this.windowName.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DragMove);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1366, 738);
            this.Controls.Add(this.windowName);
            this.Controls.Add(this.mainWindow);
            this.Controls.Add(this.audioVisualization);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = " ";
            this.TopMost = true;
            this.TransparencyKey = System.Drawing.Color.Red;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.asciiArt)).EndInit();
            this.mainWindow.ResumeLayout(false);
            this.mainWindow.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer checkPost;
        private CSAudioVisualization.AudioVisualization audioVisualization;
        private System.Windows.Forms.Panel topLBorder;
        private System.Windows.Forms.Panel leftBorder;
        private System.Windows.Forms.Panel bottomBorder;
        private System.Windows.Forms.Panel rightBorder;
        private System.Windows.Forms.Panel topRBorder;
        private System.Windows.Forms.Label exitButton;
        private System.Windows.Forms.Panel headerBorder;
        private System.Windows.Forms.Panel textboxBorder;
        private System.Windows.Forms.Label sendButton;
        private System.Windows.Forms.Label headerText;
        private System.Windows.Forms.PictureBox asciiArt;
        private System.Windows.Forms.RichTextBox inputBox;
        private System.Windows.Forms.Label label1;
        private InfoBox informationBox;
        private System.Windows.Forms.Panel mainWindow;
        private System.Windows.Forms.Label windowName;
    }
}

