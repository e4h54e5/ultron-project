﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ultron
{
    /// <summary> RichTextBox function overrides </summary>
    public static class RichTextBoxExtensions
    {
        /// resolve HideCaret() from user32.dll
        [DllImport("user32.dll")]
        static extern bool HideCaret(IntPtr hWnd);
        
        /// <summary> override RichTextBox::AppendText function to provide color support</summary>
        /// <param name="box">object to modify</param>
        /// <param name="text">text to append</param>
        /// <param name="color">color of text to append</param>
        public static void AppendText(this RichTextBox box, string text, Color color)
        {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;

            box.SelectionColor = color;
            box.AppendText(text);

            box.SelectionColor = box.ForeColor;
        }
    }

    /// <summary> custom control inheriting RichTextBox </summary>
    public class InfoBox : RichTextBox
    {
        // resolve SetSysColors from user32.dll
        [DllImport("user32.dll")]
        static extern bool SetSysColors(int cElements, int[] lpaElements, uint[] lpaRgbValues);

        // resolve HideCaret from user32.dll
        [DllImport("user32.dll")]
        static extern bool HideCaret(IntPtr hWnd);

        // declare a new speech synthesizer to read out text
        public SpeechSynthesizer synthesizer = new SpeechSynthesizer();

        // InfoBox constructor
        public InfoBox()
        {
            // set box to be read only ( non-modifiable )
            this.ReadOnly = true;
            SetStyle(ControlStyles.Selectable, false);

            // set up synthesizer to default output
            synthesizer.SetOutputToDefaultAudioDevice();
        }

        // holds message type
        public enum MsgType
        {
            Info,    // gray color
            Error,   // red color
            Warn,    // light red color
            Success, // green color
            Message  // gray color
        }

        /// <summary>appends a new message to this InfoBox</summary>
        /// <param name="type" type="MsgType">color of message author</param>
        /// <param name="subject">message author to append to InfoBox</param>
        /// <param name="message">message to append to InfoBox</param>
        public void NewMsg(MsgType type, string subject, string message)
        {
            if (message == "") return;
            
            // default subject col is a brighter white then msg
            Color col = Color.FromKnownColor(KnownColor.ControlLight);

            // change subject col based on msg type
            switch (type) {
                case MsgType.Message:
                    col = Color.FromKnownColor(KnownColor.ControlLight);
                    if (subject == "ultron") synthesizer.SpeakAsync(message);
                    break;
                case MsgType.Success:
                    col = Color.FromKnownColor(KnownColor.SeaGreen);
                    break;
                case MsgType.Error:
                    col = Color.FromKnownColor(KnownColor.IndianRed);
                    break;
            }

            // append subject with col 
            this.AppendText("[" + subject + "] ", col);

            // append message with normal col
            this.AppendText(message + "\n", Color.FromKnownColor(KnownColor.ControlDark));
        }

        /// <summary>detour for once the infobox first gains focus</summary>
        /// <param name="e">event parameters</param>
        protected override void OnEnter(EventArgs e)
        {
            // call original func
            base.OnEnter(e);

            // hide carret of infobox
            HideCaret(this.Handle);
        }

        /// <summary> detour for once the infobox gains focus </summary>
        /// <param name="e">event parameters</param>
        protected override void OnGotFocus(EventArgs e)
        {
            // call original func 
            base.OnGotFocus(e);

            // hide carret of infobox
            HideCaret(this.Handle);
        }

        /// <summary>detour for once the infobox's text has been changed</summary>
        /// <param name="e">event parameters</param>
        protected override void OnTextChanged(EventArgs e)
        {
            // scroll to bottom of page
            this.SelectionStart = this.Text.Length;
            this.ScrollToCaret();

            // call original func
            base.OnTextChanged(e);
        }

        /// <summary>detour for once a user has selected the infobox</summary>
        /// <param name="e">event parameters</param>
        protected override void OnMouseDown(MouseEventArgs e)
        {
            // call original func
            base.OnMouseDown(e);

            // on left mouse button, hide carret again
            if (e.Button == MouseButtons.Left)
                HideCaret(this.Handle);
        }
    }
}
